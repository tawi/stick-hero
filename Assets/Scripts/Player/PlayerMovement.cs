﻿using UnityEngine;
using System;
using Level.Controllers;


namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        #region Constans

        private const float MAX_FORCE = 30f;
        private const float ANTISTUCK_MAX_FORCE = 5f;
        private const float MIN_DISTANCE = 0.5f;
        private const float MAX_Y_VELOCITY = 0.3f;
        private const float MIN_X_VELOCITY = 0.3f;
        private const float MAX_STUCK_TIME = 0.2F;

        #endregion


        #region Static events

        public static event Action OnTargetAchieved = delegate { };

        #endregion


        #region Private fields

        private float currentStuckTime;
        private Vector2 targetPosition;
        private Rigidbody2D rigidbody2d;

        #endregion


        #region Public properties

        public bool IsMoving { get; private set; }
        public bool CanAchieveTarget { get; set; }

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            rigidbody2d = GetComponent<Rigidbody2D>();
            MatchController.OnResetGame += Reset;
        }


        private void OnDestroy()
        {
            rigidbody2d = GetComponent<Rigidbody2D>();
            MatchController.OnResetGame -= Reset;
        }


        private void FixedUpdate()
        {
            if (IsMoving)
            {
                if (rigidbody2d.velocity.y < MAX_Y_VELOCITY)
                {
                    rigidbody2d.AddForce(Vector2.right * MAX_FORCE, ForceMode2D.Force);

                    if (CanAchieveTarget)
                    {
                        float distance = Vector2.Distance(transform.position, targetPosition);

                        if (distance <= MIN_DISTANCE)
                        {
                            Reset();
                            OnTargetAchieved();
                        }
                    }
                }

                if (rigidbody2d.velocity.x < MIN_X_VELOCITY)
                {
                    currentStuckTime += Time.fixedDeltaTime;

                    if (currentStuckTime >= MAX_STUCK_TIME)
                    {
                        currentStuckTime = 0;
                        rigidbody2d.AddForce(Vector2.up * ANTISTUCK_MAX_FORCE, ForceMode2D.Impulse);
                    }
                }
            }
        }

        #endregion

        #region Public methods

        public void SetTarget(Vector2 newTarget)
        {
            IsMoving = true;
            targetPosition = newTarget;
            currentStuckTime = 0;
        }

        #endregion


        #region Private methods

        private void Reset()
        {
            CanAchieveTarget = true;
            rigidbody2d.velocity = Vector2.zero;
            IsMoving = false;
        }

        #endregion
    }
}
