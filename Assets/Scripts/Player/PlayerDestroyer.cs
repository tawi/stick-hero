﻿using UnityEngine;
using System;
using Level.Controllers;


namespace Player
{
    public class PlayerDestroyer : MonoBehaviour
    {
        #region Constans

        private const float MIN_Y_POSITION = -6F;
        private const float MIN_Y_LOSE_POSITION = -1F;

        #endregion


        #region Static events

        public static event Action OnLose = delegate { };

        #endregion


        #region Private fields

        private bool loseActivated = false;
        private MatchController matchController;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            matchController = FindObjectOfType<MatchController>();
            MatchController.OnResetGame += Reset;
        }


        private void OnDestroy()
        {
            MatchController.OnResetGame -= Reset;
        }


        private void FixedUpdate()
        {
            if (transform.position.y < MIN_Y_LOSE_POSITION && !loseActivated)
            {
                loseActivated = true;
                OnLose();
            }

            if (transform.position.y < MIN_Y_POSITION)
            {
                matchController.Finish();
            }
        }

        #endregion


        #region Privata methods

        private void Reset()
        {
            loseActivated = false;
        }

        #endregion
    }
}
