﻿using UnityEngine;
using UnityEngine.UI;
using Level.Controllers;


namespace UI
{
    public class MainMenuUI : MonoBehaviour
    {
        #region Serializable fields

        [SerializeField]
        private Button soundButton;
        [SerializeField]
        private Sprite soundOn, soundOff;

        #endregion


        #region Private fields

        private MatchController matchController;
        private MenusController menusController;
        private SoundManager soundManager;

        #endregion


        #region Unity lifecycle

        private void Start()
        {
            matchController = FindObjectOfType<MatchController>();
            menusController = FindObjectOfType<MenusController>();
            soundManager = FindObjectOfType<SoundManager>();
            SetSoundIcon();
        }

        #endregion
           

        #region Private methods

        private void SetSoundIcon()
        {
            soundButton.image.overrideSprite = soundManager.SoundIsEnabled ? soundOn : soundOff;
        }

        #endregion


        #region Event handlers

        public void PlayButton_OnClick()
        {
            matchController.StartGame();
            menusController.OnStartGame();
        }


        public void SoundButton_OnClick()
        {
            soundManager.SwitchSound();
            SetSoundIcon();
        }

        #endregion
    }
}
