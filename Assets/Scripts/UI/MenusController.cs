﻿using UnityEngine;
using Level.Controllers;


namespace UI
{
    public class MenusController : MonoBehaviour
    {
        #region Serializable fields

        [SerializeField]
        private GameObject MainMenuPanel;
        [SerializeField]
        private GameObject FinishMenuPanel;
        [SerializeField]
        private GameObject GameMenuPanel;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            MatchController.OnFinishGame += OnFinishGame;
            MatchController.OnResetGame += OnResetGame;
            MatchController.OnReturnToMainMenu += OnReturnToMainMenu;
        }


        private void OnDestroy()
        {
            MatchController.OnFinishGame -= OnFinishGame;
            MatchController.OnResetGame -= OnResetGame;
            MatchController.OnReturnToMainMenu -= OnReturnToMainMenu;
        }

        #endregion


        #region Public methods

        public void OnStartGame()
        {
            MainMenuPanel.SetActive(false);
            GameMenuPanel.SetActive(true);
        }

        #endregion


        #region Private methods

        private void OnFinishGame()
        {
            FinishMenuPanel.SetActive(true);
            GameMenuPanel.SetActive(false);
        }


        private void OnResetGame()
        {
            FinishMenuPanel.SetActive(false);
            GameMenuPanel.SetActive(true);
        }


        private void OnReturnToMainMenu()
        {
            MainMenuPanel.SetActive(true);
            FinishMenuPanel.SetActive(false);
            GameMenuPanel.SetActive(false);
        }

        #endregion
    }
}
