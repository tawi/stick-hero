﻿using UnityEngine;
using TMPro;
using Level.Controllers;


namespace UI
{
    public class FinishUI : MonoBehaviour
    {
        #region Serializable fields

        [SerializeField]
        private TextMeshProUGUI totalScoreValue;
        [SerializeField]
        private TextMeshProUGUI highScoreValue;

        #endregion


        #region Private fields

        private MatchController matchController;
        private ScoreManager scoreManager;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            matchController = FindObjectOfType<MatchController>();
            scoreManager = FindObjectOfType<ScoreManager>();
        }


        private void Start()
        {
            MatchController.OnFinishGame += OnFinish;
        }


        private void OnDestroy()
        {
            MatchController.OnFinishGame -= OnFinish;
        }

        #endregion


        #region Public methods

        public void OnFinish()
        {
            SetTotalScore(scoreManager.Score);
            SetHighScore(scoreManager.HighScore);
        }


        public void SetTotalScore(int Score)
        {
            totalScoreValue.SetText(Score.ToString());
        }


        public void SetHighScore(int HighScore)
        {
            highScoreValue.SetText(HighScore.ToString());
        }

        #endregion


        #region Event handlers

        public void ResetButton_OnClick()
        {
            matchController.Reset();
        }


        public void HomeButton_OnClick()
        {
            matchController.ReturnToMainMenu();
        }

        #endregion  
    }
}
