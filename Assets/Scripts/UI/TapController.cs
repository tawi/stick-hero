﻿using UnityEngine;
using Level.Sticks;


namespace UI
{
    public class TapController : MonoBehaviour
    {
        #region Private fields

        private StickHandler stickHandler;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            stickHandler = FindObjectOfType<StickHandler>();
        }

        #endregion


        #region Event handlers

        public void Tap_OnPointerDown()
        {
            stickHandler.EnableStick();
        }


        public void Tap_OnPointerUp()
        {
            stickHandler.DisableStick();
        }

        #endregion
    }
}
