﻿using UnityEngine;
using TMPro;
using Level.Controllers;


namespace UI
{
    public class GameUI : MonoBehaviour
    {
        #region Serializable fields

        [SerializeField]
        private TextMeshProUGUI gameScoreValue;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            ScoreManager.OnScoreChanged += SetScore;
        }


        private void OnDestroy()
        {
            ScoreManager.OnScoreChanged -= SetScore;
        }

        #endregion


        #region Private methods

        private void SetScore(int Score)
        {
            gameScoreValue.SetText(Score.ToString());
        }

        #endregion
    }
}
