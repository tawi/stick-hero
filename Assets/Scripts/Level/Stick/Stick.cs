﻿using UnityEngine;
using System;


namespace Level.Sticks
{
    public class Stick : MonoBehaviour
    {
        #region Constans

        private const float SHIFT_ANGLE = -90F;
        private const float ROTATION_SPEED = 4F;
        private const float EPS = 0.001f;

        #endregion


        #region Static events

        public static event Action OnEndRotation = delegate { };
        public static event Action OnEndRotationSound = delegate { };

        #endregion


        #region Private fields

        private float currentTargetAngle = 0f;
        private LineRenderer lineRenderer;
        private BoxCollider2D boxCollider2D;

        #endregion


        #region Public properties

        public float Width
        {
            get
            {
                return lineRenderer.endWidth;
            }
        }


        public bool IsRotating { get; private set; }

        #endregion
             
        
        #region Unity lifecycle

        private void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
            boxCollider2D = GetComponent<BoxCollider2D>();
            boxCollider2D.size = new Vector2(lineRenderer.startWidth / 4, boxCollider2D.size.y);
            boxCollider2D.offset = new Vector2(lineRenderer.startWidth / 8, boxCollider2D.offset.y);
        }


        private void FixedUpdate()
        {
            if (IsRotating)
            {
                Quaternion targetQuatertion = Quaternion.Euler(0, 0, currentTargetAngle);

                if (!AlmostEqual(transform.rotation, targetQuatertion, EPS))
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, targetQuatertion, ROTATION_SPEED);
                }
                else
                {
                    transform.rotation = targetQuatertion;
                    IsRotating = false;
                    OnEndRotation();

                    if (currentTargetAngle >= -90)
                    {
                        OnEndRotationSound();
                    }
                }
            }
        }

        #endregion


        #region Public methods

        public bool AlmostEqual(Quaternion val, Quaternion about, float range)
        {
            return (Quaternion.Dot(val, about) > 1f - range);
        }


        public void SetLineWidth(float lineHeight)
        {
            float currentHeight = lineRenderer.GetPosition(1).y;
            lineRenderer.SetPosition(1, new Vector3(0, currentHeight + lineHeight));
        }


        public void CreateCollider()
        {
            float lineHeight = lineRenderer.GetPosition(1).y;
            boxCollider2D.size = new Vector2(boxCollider2D.size.x, lineHeight);
            boxCollider2D.offset = new Vector2(boxCollider2D.offset.x, lineHeight / 2);
        }


        public void Reset()
        {
            lineRenderer.SetPosition(1, Vector3.zero);
            currentTargetAngle = 0;
            transform.rotation = Quaternion.identity;
        }


        public Vector3 GetHeadPoint()
        {
            Vector3 headPoint = transform.position + Quaternion.Euler(0, 0, currentTargetAngle) * lineRenderer.GetPosition(1);
            return headPoint;
        }


        public void NextAngle()
        {
            IsRotating = true;
            currentTargetAngle += SHIFT_ANGLE;
        }

        #endregion
    }
}
