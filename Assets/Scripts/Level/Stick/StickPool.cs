﻿using System.Collections.Generic;
using UnityEngine;
using Level.Controllers;


namespace Level.Sticks
{
    public class StickPool : MonoBehaviour
    {
        #region Constans

        private const int MAX_STICK_COUNT = 2;
        private const string POOL_NAME = "StickPool";

        #endregion


        #region Serializable fields

        [SerializeField]
        private Stick stickPrefab;

        #endregion


        #region Private fields

        private int currentStickIndex = 0;
        private List<Stick> sticks = new List<Stick>();

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            GenerateSticks();
            MatchController.OnResetGame += Reset;
        }


        private void OnDestroy()
        {
            MatchController.OnResetGame -= Reset;
        }

        #endregion


        #region Public methods

        public Stick GetStick()
        {
            var stick = sticks[currentStickIndex];
            stick.Reset();
            stick.gameObject.SetActive(true);

            currentStickIndex = currentStickIndex + 1 >= sticks.Count ? 0 : currentStickIndex + 1;

            return stick;
        }


        public void Reset()
        {
            for (int i = 0; i < sticks.Count; i++)
            {
                sticks[i].gameObject.SetActive(false);
            }

            currentStickIndex = 0;
        }

        #endregion


        #region Private methods

        private void GenerateSticks()
        {
            Transform stickPool = new GameObject(POOL_NAME).transform;

            for (int i = 0; i < MAX_STICK_COUNT; i++)
            {
                var stickTemp = CreateStick();
                sticks.Add(stickTemp);
                stickTemp.gameObject.SetActive(false);
                stickTemp.transform.parent = stickPool;
            }
        }


        private Stick CreateStick()
        {
            return Instantiate(stickPrefab.gameObject).GetComponent<Stick>();
        }

        #endregion
    }
}
