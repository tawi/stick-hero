﻿using UnityEngine;
using Level.Controllers;
using Level.Platforms;
using Player;


namespace Level.Sticks
{
    public class StickHandler : MonoBehaviour
    {
        #region Constans

        private float GROWTH_SPEED_PER_FRAME = 0.07f;

        #endregion


        #region Private fields

        private StickPool stickPool;
        private Stick currentStick;
        private PlatformHandler platformHandler;
        private MatchController matchController;
        private ScoreManager scoreManager;
        private SoundManager soundManager;

        #endregion


        #region Public properties

        public bool StickIsEnabled { get; private set; } = false;
        public bool HandlerIsLocked { get; set; }

        #endregion
           

        #region Unity lifecycle

        private void Awake()
        {
            stickPool = FindObjectOfType<StickPool>();
            platformHandler = FindObjectOfType<PlatformHandler>();
            matchController = FindObjectOfType<MatchController>();
            scoreManager = FindObjectOfType<ScoreManager>();
            soundManager = FindObjectOfType<SoundManager>();
            Stick.OnEndRotation += OnEndRotation;
            MatchController.OnResetGame += Reset;
            PlayerDestroyer.OnLose += OnLose;
        }      


        private void OnDestroy()
        {
            Stick.OnEndRotation -= OnEndRotation;
            MatchController.OnResetGame -= Reset;
            PlayerDestroyer.OnLose -= OnLose;
        }


        private void FixedUpdate()
        {
            bool state = StickIsEnabled && matchController.GameIsEnabled;

            soundManager.OnStickGrowthSoundPlay(state);

            if (state)
            {
                if (currentStick != null)
                {
                    currentStick.SetLineWidth(GROWTH_SPEED_PER_FRAME);
                }
            }
        }

        #endregion


        #region Public methods

        public void EnableStick()
        {
            if (!StickIsEnabled && matchController.GameIsEnabled && !HandlerIsLocked)
            {
                StickIsEnabled = true;
                HandlerIsLocked = true;

                currentStick = stickPool.GetStick();

                Vector3 halfWidthShift = new Vector3(-currentStick.Width / 2, 0);
                Vector2 spawnPosition = platformHandler.GetCurrentPlatform().RightCorner + halfWidthShift;

                currentStick.transform.position = spawnPosition;
            }
        }


        public void DisableStick()
        {
            if (StickIsEnabled && matchController.GameIsEnabled)
            {
                StickIsEnabled = false;
                currentStick.NextAngle();
                currentStick.CreateCollider();
            }
        }

        #endregion


        #region Private methods

        private void OnEndRotation()
        {
            var nextPlatform = platformHandler.GetNextPlatform();

            var headPoint = currentStick.GetHeadPoint();

            bool canAchieveTarget = true;

            if (nextPlatform.LeftCenterCorner.x <= headPoint.x && nextPlatform.RightCenterCorner.x >= headPoint.x)
            {
                scoreManager.AddExtraScore();
            }
            else if (nextPlatform.LeftCorner.x <= headPoint.x && nextPlatform.RightCorner.x >= headPoint.x)
            {
                scoreManager.AddCommonScore();
            }
            else
            {
                canAchieveTarget = false;
            }

            matchController.OnEndStickRotation(canAchieveTarget);
        }


        private void OnLose()
        {
            if (currentStick != null)
            {
                currentStick.NextAngle();
            }
        }


        private void Reset()
        {
            StickIsEnabled = false;
            HandlerIsLocked = false;
            currentStick = null;
        }

        #endregion
    }
}
