﻿using UnityEngine;
using Level.Controllers;


namespace Level
{
    public class CameraController : MonoBehaviour
    {
        #region Constans

        private float CAMERA_SPEED = 0.3F;
        private readonly float ZPosition = -10f;

        #endregion


        #region Private fields

        private Vector3 startPosition;
        private Vector3 targetPosition;
        private MatchController matchController;

        #endregion


        #region Public properties

        public Vector3 TargetPosition
        {
            get
            {
                return targetPosition;
            }
            set
            {
                Vector3 newTargetPosition = value;
                newTargetPosition.z = ZPosition;
                targetPosition = newTargetPosition;
            }
        }


        public float VerticalEdge { get; private set; }
        public float HorizontalEdge { get; private set; }

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            VerticalEdge = Camera.main.orthographicSize;
            HorizontalEdge = VerticalEdge * Screen.width / Screen.height;
            TargetPosition = Vector3.zero;
            matchController = FindObjectOfType<MatchController>();
            startPosition = transform.position;
        }


        private void FixedUpdate()
        {
            if (matchController.GameIsEnabled)
            {
                transform.position = Vector3.MoveTowards(transform.position, TargetPosition, CAMERA_SPEED);
            }
        }

        #endregion


        #region Public methods

        public void ForceMove()
        {
            transform.position = TargetPosition;
        }


        public void BackToMenu()
        {
            TargetPosition = startPosition;
            ForceMove();
        }

        #endregion
    }
}
