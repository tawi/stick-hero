﻿using UnityEngine;


namespace Level.Platforms
{
    public class Platform : MonoBehaviour
    {
        #region Public properties

        public PlatformSettings Settings { get; set; }


        public Vector3 LeftCorner
        {
            get
            {
                return transform.position + Settings.LeftCornerShift;
            }
        }


        public Vector3 Center
        {
            get
            {
                return transform.position + Settings.CenterShift;
            }
        }


        public Vector3 RightCorner
        {
            get
            {
                return transform.position + Settings.RightCornerShift;
            }
        }


        public Vector3 LeftCenterCorner
        {
            get
            {
                return transform.position + Settings.CenterShift - new Vector3(Settings.CenterWidth / 2, 0);
            }
        }


        public Vector3 RightCenterCorner
        {
            get
            {
                return transform.position + Settings.CenterShift + new Vector3(Settings.CenterWidth / 2, 0);
            }
        }

        #endregion
    }
}
