﻿using UnityEngine;


namespace Level.Platforms
{
    public class PlatformCreator : MonoBehaviour
    {
        #region Constans

        private const float MAX_WIDTH_PER_SQUARE = 0.2f;
        private const float MAX_HEIGHT = 4f;
        private const float CENTER_WIDTH = 0.15f;
        private const float CENTER_Height = 0.10f;

        #endregion


        #region Serializable fields

        [SerializeField]
        private Platform platformPrefab;

        #endregion


        #region Public methods

        public Platform CreatePlaform(int squareCount)
        {
            if (squareCount >= 1)
            {
                LineRenderer platformLine = Instantiate(platformPrefab.gameObject).GetComponent<LineRenderer>();

                string platformName = "Platform" + squareCount.ToString();
                platformLine.gameObject.name = platformName;

                float platformWidth = squareCount * MAX_WIDTH_PER_SQUARE;

                platformLine = SetWidth(platformLine, platformWidth);
                platformLine = PaintPlatform(platformLine, MAX_HEIGHT);
                platformLine = CreateCollider(platformLine, platformWidth);

                CreatePlaformCenter(platformLine.transform, platformLine.material);

                var platform = platformLine.GetComponent<Platform>();

                platform = SetPlatformSettings(platform, squareCount, platformWidth);

                return platform;
            }
            else
            {
                return null;
            }
        }

        #endregion


        #region Private Methods

        private LineRenderer SetWidth(LineRenderer line, float width)
        {
            line.startWidth = width;
            line.endWidth = width;
            return line;
        }


        private LineRenderer PaintPlatform(LineRenderer platformLine, float height)
        {
            Vector2 platformTopEdgePosition = new Vector2(0, height);

            platformLine.SetPosition(0, Vector2.zero);
            platformLine.SetPosition(1, platformTopEdgePosition);

            return platformLine;
        }


        private LineRenderer CreateCollider(LineRenderer platformLine, float platformWidth)
        {
            var boxCollider = platformLine.GetComponent<BoxCollider2D>();
            boxCollider.size = new Vector2(platformWidth, MAX_HEIGHT);
            return platformLine;
        }


        private Platform SetPlatformSettings(Platform platform, int squareCount, float platformWidth)
        {
            Vector2 leftCorner = new Vector2(-platformWidth / 2, MAX_HEIGHT);
            Vector2 rightCorner = new Vector2(platformWidth / 2, MAX_HEIGHT);
            Vector2 platformCenter = new Vector2(0, MAX_HEIGHT);

            PlatformSettings platformSettings = new PlatformSettings(squareCount, platformWidth, CENTER_WIDTH, leftCorner, platformCenter, rightCorner);
            platform.Settings = platformSettings;

            return platform;
        }


        private void CreatePlaformCenter(Transform platformParent, Material lineMaterial)
        {
            LineRenderer platformCenter = new GameObject("PlatformCenter").AddComponent<LineRenderer>();

            platformCenter = PaintPlatform(platformCenter, -CENTER_Height);
            platformCenter = SetWidth(platformCenter, CENTER_WIDTH);

            Color32 platformColor = Color.red;
            platformCenter.startColor = platformColor;
            platformCenter.endColor = platformColor;
            platformCenter.material = lineMaterial;
            platformCenter.sortingOrder = 1;
            platformCenter.useWorldSpace = false;

            platformCenter.transform.parent = platformParent;
            platformCenter.transform.localPosition = new Vector2(0, MAX_HEIGHT);
        }

        #endregion
    }
}
