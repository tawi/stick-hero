﻿using UnityEngine;


namespace Level.Platforms
{
    public class PlatformSettings
    {
        #region Public properties

        public int SquareCount { get; private set; }
        public float Width { get; private set; }
        public float CenterWidth { get; private set; }
        public Vector3 LeftCornerShift { get; private set; }
        public Vector3 CenterShift { get; private set; }
        public Vector3 RightCornerShift { get; private set; }

        #endregion


        #region Constructor

        public PlatformSettings(int squareCount, float width, float centerWidth, Vector3 leftCorner, Vector3 center, Vector3 rightCorner)
        {
            SquareCount = squareCount;
            Width = width;
            CenterWidth = centerWidth;
            LeftCornerShift = leftCorner;
            CenterShift = center;
            RightCornerShift = rightCorner;
        }

        #endregion
    }
}
