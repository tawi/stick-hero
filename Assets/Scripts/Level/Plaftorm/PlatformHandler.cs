﻿using UnityEngine;


namespace Level.Platforms
{
    public class PlatformHandler : MonoBehaviour
    {
        #region Constans

        private const int START_PLATFORM_SIZE = 10;
        private const float MIN_SHIFT_FROM_PLATFORM = 0.3f;
        private const float PLATFORM_MOVE_SPEED = 0.35f;
        private const float MINUMUM_DISTANCE = 0.001f;

        #endregion


        #region Private fields

        private Vector2 targetPosition;
        private CameraController cameraController;
        private PlatformPool platformPool;
        private Platform previousPlatform;
        private Platform currentPlatform;
        private Platform nextPlaform;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            platformPool = FindObjectOfType<PlatformPool>();
            cameraController = FindObjectOfType<CameraController>();
        }


        private void Start()
        {
            Reset();
        }


        private void FixedUpdate()
        {
            MoveToTargetPosition();
        }

        #endregion


        #region Public methods

        public void NextPlaform()
        {
            if (nextPlaform != null)
            {
                previousPlatform = currentPlatform;
                currentPlatform = nextPlaform;
            }

            nextPlaform = platformPool.GetRandomPlatform();

            Vector2 cameraPosition = cameraController.TargetPosition;
            Vector2 platformShift = new Vector2(cameraController.HorizontalEdge, -cameraController.VerticalEdge);
            Vector2 platformWidth = new Vector2(nextPlaform.Settings.Width, 0);

            nextPlaform.transform.position = cameraPosition + platformShift + platformWidth;

            targetPosition = GetTargetPosition(nextPlaform.Settings.Width);
        }


        public void Reset()
        {
            previousPlatform = null;
            nextPlaform = null;
            currentPlatform = null;
            CreateStartPlatform();
        }


        public Platform GetCurrentPlatform()
        {
            return currentPlatform;
        }


        public Platform GetNextPlatform()
        {
            return nextPlaform;
        }

        #endregion


        #region Private methods

        private void MoveToTargetPosition()
        {
            if (nextPlaform != null)
            {
                nextPlaform.transform.position = Vector2.MoveTowards(nextPlaform.transform.position, targetPosition, PLATFORM_MOVE_SPEED);

                float distance = Vector2.Distance(nextPlaform.transform.position, targetPosition);

                if (distance < MINUMUM_DISTANCE)
                {
                    if (previousPlatform != null)
                    {
                        previousPlatform.gameObject.SetActive(false);
                    }
                }
            }
        }


        private void CreateStartPlatform()
        {
            currentPlatform = platformPool.GetFixedPlaform(START_PLATFORM_SIZE);
            SetPlatformPosition(currentPlatform);
        }


        private void SetPlatformPosition(Platform platform)
        {
            platform.transform.position = new Vector2(0, -cameraController.VerticalEdge);
        }


        private Vector2 GetTargetPosition(float nextPlatformWidth)
        {
            Vector2 cameraPosition = cameraController.TargetPosition;

            Vector3 shiftFromPlatform = new Vector3(MIN_SHIFT_FROM_PLATFORM, 0);
            float startPosition = (currentPlatform.RightCorner + shiftFromPlatform).x + nextPlatformWidth / 2;
            float endPosition = cameraPosition.x + cameraController.HorizontalEdge - nextPlatformWidth / 2;

            float xTargetPosition = Random.Range(startPosition, endPosition);

            targetPosition = new Vector2(xTargetPosition, -cameraController.VerticalEdge);

            return targetPosition;
        }

        #endregion
    }
}
