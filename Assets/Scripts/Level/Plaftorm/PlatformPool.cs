﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Level.Controllers;


namespace Level.Platforms
{
    public class PlatformPool : MonoBehaviour
    {
        #region Constans

        private const int MIN_SQUARE_COUNT = 2;
        private const int MAX_SQUARE_COUNT = 10;
        private const int MAX_PLATFORMS_PER_SQUARE = 3;
        private const string POOL_NAME = "PlatformPool";

        #endregion


        #region Private fields

        private PlatformCreator platformCreator;
        private List<Platform> pool = new List<Platform>();

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            platformCreator = GetComponent<PlatformCreator>();
            GeneratePool();
            MatchController.OnResetGame += Reset;
        }


        private void OnDestroy()
        {
            MatchController.OnResetGame -= Reset;
        }

        #endregion


        #region Public methods

        public Platform GetFixedPlaform(int squareCount)
        {
            var platform = pool.FirstOrDefault(item => item.gameObject.activeSelf == false && item.Settings.SquareCount == squareCount);
            platform.gameObject.SetActive(true);
            return platform;
        }


        public Platform GetRandomPlatform()
        {
            int randomSquareCount = Random.Range(MIN_SQUARE_COUNT, MAX_SQUARE_COUNT);

            var platform = GetFixedPlaform(randomSquareCount);
            return platform;
        }

        #endregion


        #region Private methods

        private void GeneratePool()
        {
            Transform poolParent = new GameObject(POOL_NAME).transform;

            for (int i = MIN_SQUARE_COUNT; i <= MAX_SQUARE_COUNT; i++)
            {
                for (int j = 0; j < MAX_PLATFORMS_PER_SQUARE; j++)
                {
                    var platform = platformCreator.CreatePlaform(i);

                    pool.Add(platform);

                    platform.gameObject.SetActive(false);
                    platform.transform.parent = poolParent;
                }
            }
        }


        private void Reset()
        {
            for (int i = 0; i < pool.Count; i++)
            {
                pool[i].gameObject.SetActive(false);
            }
        }

        #endregion
    }
}
