﻿using UnityEngine;
using System;


namespace Level.Controllers
{
    public class ScoreManager : MonoBehaviour
    {
        #region Constans

        private const string HIGHSCORE_KEY = "HighScore";
        private const int COMMON_SCORE = 1;
        private const int EXTRA_SCORE = 2;

        #endregion


        #region Static events

        public static event Action<int> OnScoreChanged = delegate { };

        #endregion


        #region Public properties

        public int Score { get; private set; }
        public int HighScore { get; private set; }

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            if (PlayerPrefs.HasKey(HIGHSCORE_KEY))
            {
                HighScore = PlayerPrefs.GetInt(HIGHSCORE_KEY);
            }

            MatchController.OnFinishGame += Finish;
            MatchController.OnResetGame += Reset;
        }


        private void OnDestroy()
        {
            MatchController.OnFinishGame -= Finish;
            MatchController.OnResetGame -= Reset;
        }

        #endregion


        #region Public methods

        public void Finish()
        {
            if (Score > HighScore)
            {
                HighScore = Score;
                SaveHighScore();
            }
        }


        public void AddCommonScore()
        {
            Score += COMMON_SCORE;
            OnScoreChanged(Score);
        }


        public void AddExtraScore()
        {
            Score += EXTRA_SCORE;
            OnScoreChanged(Score);
        }

        #endregion


        #region Private methods

        private void SaveHighScore()
        {
            PlayerPrefs.SetInt(HIGHSCORE_KEY, HighScore);
            PlayerPrefs.Save();
        }


        private void Reset()
        {
            Score = 0;
            OnScoreChanged(Score);
        }

        #endregion
    }
}