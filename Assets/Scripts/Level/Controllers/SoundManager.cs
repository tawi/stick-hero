﻿using UnityEngine;
using Player;
using Level.Sticks;


namespace Level.Controllers
{
    public class SoundManager : MonoBehaviour
    {
        #region Constans

        private const float STICK_PITCH_RATE = 2F;
        private const string SOUND_KEY = "SoundIsEnabled";

        #endregion


        #region Serializable fields

        [SerializeField]
        private AudioClip achieveSound;
        [SerializeField]
        private AudioClip hitSound;
        [SerializeField]
        private AudioClip loseSound;

        #endregion


        #region Private fields

        private float startPitch;
        private AudioSource source;

        #endregion


        #region Public properies

        public bool SoundIsEnabled { get; private set; }

        #endregion
           
        
        #region Unity lifecycle

        private void Awake()
        {
            source = GetComponent<AudioSource>();
            startPitch = source.pitch;
  
            if (PlayerPrefs.HasKey(SOUND_KEY))
            {
                SoundIsEnabled = (PlayerPrefs.GetInt(SOUND_KEY) == 1);
            }
            else
            {
                SoundIsEnabled = true;
            }

            PlayerMovement.OnTargetAchieved += OnAchieveSoundPlay;
            PlayerDestroyer.OnLose += OnLoseSoundPlay;
            Stick.OnEndRotationSound += OnEndRotationSoundPlay;
        }


        private void OnDestroy()
        {
            PlayerMovement.OnTargetAchieved -= OnAchieveSoundPlay;
            PlayerDestroyer.OnLose -= OnLoseSoundPlay;
            Stick.OnEndRotationSound -= OnEndRotationSoundPlay;
        }

        #endregion


        #region Public methods

        public void OnStickGrowthSoundPlay(bool state)
        {
            if (state && !source.isPlaying)
            {
                source.Play();
                source.pitch = STICK_PITCH_RATE;
            }
            else if (!state && source.isPlaying)
            {
                source.pitch = startPitch;
            }
        }


        public void OnEndRotationSoundPlay()
        {
            source.PlayOneShot(hitSound);
        }


        public void OnAchieveSoundPlay()
        {
            source.PlayOneShot(achieveSound);
        }


        public void OnLoseSoundPlay()
        {
            source.PlayOneShot(loseSound);
        }


        public void SwitchSound()
        {
            SoundIsEnabled = !SoundIsEnabled;
            AudioListener.pause = !SoundIsEnabled;
            SaveSoundState();
        }


        public void SaveSoundState()
        {
            int stateValue = SoundIsEnabled ? 1 : 0;
            PlayerPrefs.SetInt(SOUND_KEY, stateValue);
            PlayerPrefs.Save();
        }

        #endregion
    }
}
