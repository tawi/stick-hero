﻿using UnityEngine;
using System;
using Level.Platforms;
using Level.Sticks;
using Player;


namespace Level.Controllers
{
    public class MatchController : MonoBehaviour
    {
        #region Static events
        public static event Action OnFinishGame = delegate { };
        public static event Action OnResetGame = delegate { };
        public static event Action OnReturnToMainMenu = delegate { };
        #endregion


        #region Private fields

        private Vector3 cameraRelativeCurrentPlaformCenter;
        private bool newGame = true;
        private PlatformHandler platformHandler;
        private CameraController cameraController;
        private PlayerMovement playerMovement;
        private StickHandler stickHandler;

        #endregion


        #region Public properties

        public bool GameIsEnabled { get; private set; } = false;

        #endregion


        #region Unity lifecycle

        private void Awake()
        {
            platformHandler = FindObjectOfType<PlatformHandler>();
            cameraController = FindObjectOfType<CameraController>();
            playerMovement = FindObjectOfType<PlayerMovement>();
            stickHandler = FindObjectOfType<StickHandler>();
            PlayerMovement.OnTargetAchieved += OnTargetAchieved;
        }


        private void OnDestroy()
        {
            PlayerMovement.OnTargetAchieved -= OnTargetAchieved;
        }

        #endregion


        #region Public methods

        public void StartGame()
        {
            GameIsEnabled = true;

            var platform = platformHandler.GetCurrentPlatform();
            ResetPlayer(platform);

            Vector3 platformPosition = platform.transform.position;
            Vector3 cameraEdge = new Vector3(cameraController.HorizontalEdge, cameraController.VerticalEdge);
            Vector3 platformHalfWidth = new Vector3(platform.Settings.Width / 2, 0);

            Vector3 targetPosition = platformPosition + cameraEdge - platformHalfWidth;
            cameraController.TargetPosition = targetPosition;

            if (newGame)
            {
                newGame = !newGame;
            }
            else
            {
                cameraController.ForceMove();
            }

            cameraRelativeCurrentPlaformCenter = cameraController.TargetPosition - platform.Center;
            cameraRelativeCurrentPlaformCenter.z = 0;

            platformHandler.NextPlaform();
        }


        public void OnEndStickRotation(bool canAchieveTarget)
        {
            Vector2 targetPosition = platformHandler.GetNextPlatform().RightCorner;
            playerMovement.CanAchieveTarget = canAchieveTarget;
            playerMovement.SetTarget(targetPosition);
        }


        public void Finish()
        {
            GameIsEnabled = false;
            OnFinishGame();
        }


        public void ReturnToMainMenu()
        {
            GameIsEnabled = false;
            PartialReset();
            cameraController.BackToMenu();

            newGame = true;
            Platform platform = platformHandler.GetCurrentPlatform();
            ResetPlayer(platform);
            OnReturnToMainMenu();
        }


        public void Reset()
        {
            PartialReset();
            StartGame();
        }

        #endregion


        #region Private methods

        private void OnTargetAchieved()
        {
            var currentPlatformCenter = platformHandler.GetNextPlatform().Center;

            Vector3 targetPosition = currentPlatformCenter + cameraRelativeCurrentPlaformCenter;

            cameraController.TargetPosition = targetPosition;
            stickHandler.HandlerIsLocked = false;
            platformHandler.NextPlaform();
        }


        private void ResetPlayer(Platform platform)
        {
            playerMovement.transform.position = platform.Center;
        }


        private void PartialReset()
        {
            OnResetGame();
            platformHandler.Reset();
        }

        #endregion
    }
}
